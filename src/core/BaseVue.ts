import Vue from 'vue';
import { Component } from 'vue-property-decorator';
@Component
export default class BaseVue extends Vue {
    constructor() {
        super();
     //   this.$validator.localize('es');
    }
  
    operationSuccess(message: string = "Operación exitosa!", duration = 2000) {
        this.$buefy.toast.open({
            message: message,
            type: 'is-success',
            duration: duration
        });
    }
    operationPublish(message: string = "Convocatoria publicada con éxito!", duration = 2000) {
        this.$buefy.toast.open({
            message: message,
            type: 'is-success',
            duration: duration
        });
    }
    noPublish(message: string = "Convocatoria rechazada con éxito!", duration = 2000) {
        this.$buefy.toast.open({
            message: message,
            type: 'is-success',
            duration: duration
        });
    }

    operationFailed(message: string = "Operación fallida!", duration = 4000) {
        this.$buefy.toast.open({
            message: message,
            type: 'is-danger',
            position: "is-top-right",
            duration: duration
        });
    }
    operationNoAllowed(message: string = "Operación no permitida!", duration = 4000) {
        this.$buefy.toast.open({
            message: message,
            type: 'is-warning',
            duration: duration,
            position: "is-bottom",
        });
    }

    operationFailedErrors(errors: string[], queue = false, duration = 4000, type = "is-danger") {
        errors.forEach(e => this.$buefy.toast.open({
            message: e,
            duration: duration,
            queue: queue,
            type: type,
            position: "is-top-right"
        }))
    }

  

    
}

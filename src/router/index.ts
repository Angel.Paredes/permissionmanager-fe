import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import BaseView from '../views/base.vue';

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: "/player",
    component: BaseView,
    children: [
      {
        path: "/player",
        component: () => import(/* webpackChunkName: "about" */ '../modules/player/table.vue'),
      },
      {
        path: "/player/agregar",
        name: "Agregar",
        component:  () => import(/* webpackChunkName: "about" */ '../modules/player/add.vue'),
      },
      {
        path: "/player/editar/:id",
        name: "Editar",
        component:  () => import(/* webpackChunkName: "about" */ '../modules/player/edit.vue'),
      },
      {
        path: "/Event",
        component: () => import(/* webpackChunkName: "about" */ '../modules/Event/table.vue'),
      },
      {
        path: "/Event/agregar",
        name: "Agregar",
        component:  () => import(/* webpackChunkName: "about" */ '../modules/Event/add.vue'),
      },
      {
        path: "/Event/editar/:id",
        name: "Editar",
        component:  () => import(/* webpackChunkName: "about" */ '../modules/Event/edit.vue'),
      },
      {
        path: "/StreamVideo",
        component: () => import(/* webpackChunkName: "about" */ '../modules/StreamVideo/table.vue'),
      },
      {
        path: "/StreamVideo/agregar",
        name: "Agregar",
        component:  () => import(/* webpackChunkName: "about" */ '../modules/StreamVideo/add.vue'),
      },
      {
        path: "/StreamVideo/editar/:id",
        name: "Editar",
        component:  () => import(/* webpackChunkName: "about" */ '../modules/StreamVideo/edit.vue'),
      },
      {
        path: "/Banner",
        component: () => import(/* webpackChunkName: "about" */ '../modules/Banner/table.vue'),
      },
      {
        path: "/Banner/agregar",
        name: "Agregar",
        component:  () => import(/* webpackChunkName: "about" */ '../modules/Banner/add.vue'),
      },
      {
        path: "/Association",
        component: () => import(/* webpackChunkName: "about" */ '../modules/association/table.vue'),
      },
      {
        path: "/Association/agregar",
        name: "Agregar",
        component:  () => import(/* webpackChunkName: "about" */ '../modules/association/add.vue'),
      },
      {
        path: "/Association/editar/:id",
        name: "Editar",
        component:  () => import(/* webpackChunkName: "about" */ '../modules/association/edit.vue'),
      },
      {
        path: "/Ranking",
        component: () => import(/* webpackChunkName: "about" */ '../modules/ranking/table.vue'),
      },
      {
        path: "/Ranking/agregar",
        name: "Agregar",
        component:  () => import(/* webpackChunkName: "about" */ '../modules/ranking/add.vue'),
      },
      {
        path: "/Ranking/editar/:id",
        name: "Editar",
        component:  () => import(/* webpackChunkName: "about" */ '../modules/ranking/edit.vue'),
      },
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import Axios from 'axios';
import VeeValidate from 'vee-validate';
import './globalFilters';
import './globalcomponents'
import './css/style.css'
Vue.use(VeeValidate)
Vue.use(Buefy)

Axios.defaults.baseURL = 'https://localhost:44359/';

Vue.config.productionTip = false;

window.onload = () => {
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
}
import Vue from "vue";
import moment from 'moment';

Vue.filter("DateTime", (value: string) =>{
  return  moment(value).format("DD/MM/YYYY hh:mm a")});

import StreamVideoModel from "@/models/StreamVideo.model";
import axios from "axios";

export default class StreamVideoService {
  
  async saveStreamVideo(StreamVideo: StreamVideoModel) {
    return await axios.post("StreamVideo", StreamVideo);
  }

  async updateStreamVideo(id: number, StreamVideo: StreamVideoModel) {
    return await axios.put("StreamVideo/" + id, StreamVideo);
  }

  async deleteStreamVideo(id: number) {
    return await axios.delete("StreamVideo/" + id);
  }

  async getStreamVideo() {
    return await axios.get("StreamVideo");
  }

  async getStreamVideoById(id: number) {
    return await axios.get("StreamVideo/" + id);
  }
 
}
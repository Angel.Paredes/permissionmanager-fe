import EventModel from "@/models/event.model";
import axios from "axios";Event

export default class EventService {
  
  async saveEvent(event: EventModel) {
    return await axios.post("Event", event);
  }

  async updateEvent(id: number, event: EventModel) {
    return await axios.put("Event/" + id, event);
  }

  async deleteEvent(id: number) {
    return await axios.delete("Event/" + id);
  }

  async getEvent() {
    return await axios.get("Event");
  }

  async getEventById(id: number) {
    return await axios.get("Event/" + id);
  }
 
}

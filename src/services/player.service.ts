import Player from "../models/player/player.model";
import axios from "axios";

export default class PlayerService {
  
  async savePlayer(Player: Player) {
    return await axios.post("Player", Player);
  }

  async updatePlayer(id: number, Player: Player) {
    return await axios.put("Player/" + id, Player);
  }

  async deletePlayer(id: number) {
    return await axios.delete("Player/" + id);
  }

  async getPlayer() {
    return await axios.get("Player");
  }

  async getPlayerById(id: number) {
    return await axios.get("Player/" + id);
  }
  async getMan() {
    return await axios.get("Player/ManPlayer");
  }
  async getWoman() {
    return await axios.get("Player/WomanPlayer");
  }
 
}

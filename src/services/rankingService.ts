import Ranking from "@/models/ranking.model";
import axios from "axios";

export default class RankingService {
  
  async saveRanking(Ranking: Ranking) {
    return await axios.post("Ranking", Ranking);
  }

  async updateRanking(id: number, Ranking: Ranking) {
    return await axios.put("Ranking/" + id, Ranking);
  }

  async deleteRanking(id: number) {
    debugger
    return await axios.delete("Ranking/" + id);
  }

  async getRanking() {
    return await axios.get("Ranking");
  }

  async getRankingById(id: number) {
    return await axios.get("Ranking/" + id);
  }
 
}

import AssociationModel from "@/models/association.model";
import axios from "axios";

export default class AssociationService {
  
  async saveAssociation(Association: AssociationModel) {
    return await axios.post("Association", Association);
  }

  async updateAssociation(id: number, Association: AssociationModel) {
    return await axios.put("Association/" + id, Association);
  }

  async deleteAssociation(id: number) {
    debugger
    return await axios.delete("Association/" + id);
  }

  async getAssociation() {
    return await axios.get("Association");
  }

  async getAssociationById(id: number) {
    return await axios.get("Association/" + id);
  }
 
}

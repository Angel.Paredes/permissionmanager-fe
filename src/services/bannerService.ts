import Banner from "@/models/banner.model";
import axios from "axios";

export default class BannerService {
  
  async saveBanner(Banner: Banner) {
    return await axios.post("Banner", Banner);
  }

  async updateBanner(id: number, Banner: Banner) {
    return await axios.put("Banner/" + id, Banner);
  }

  async deleteBanner(id: number) {
    debugger
    return await axios.delete("Banner/" + id);
  }

  async getBanner() {
    return await axios.get("Banner");
  }

  async getBannerById(id: number) {
    return await axios.get("Banner/" + id);
  }
 
}

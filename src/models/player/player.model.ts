import { GenderType } from '../../enums/genderType';
export default class Player{
    public id?: number | null;
    public name?: string | null;
    public lastName?: string | null;
    public fullName?: string | null;

    public nationality?: string | null;
    public genderType?: GenderType | null;
    public ranking: number | null = 0;
    public birdDate?: Date | null;

    public age:number | null = 0;
    public image:any = [];
    public file:any = null;
    constructor(){
        this.init();
    }

    init(){
        this.id = null;
        this.name = null;
        this.lastName = null;
        this.ranking = null;
        this.age = null;
        this.file = null;
        this.nationality = "";
        this.genderType = null;
        this.birdDate = new Date();
        this.image = [];
    }
}
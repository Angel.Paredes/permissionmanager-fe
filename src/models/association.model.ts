export default class AssociationModel {
    public id?: number | null;
    public trainer?: string | null;
    public city?: string | null;
    public phoneNumber?: string | null;
    public address?: string | null;
    public longitude?: number | null;
    public latitude?: number | null;


    constructor(){
        this.init();
    }

    init(){
        this.id = 0;
        this.address = null;
        this.city = null;
        this.phoneNumber = null;
        this.trainer = null;
        this.longitude = 0;
        this.latitude = 0;
    }
}
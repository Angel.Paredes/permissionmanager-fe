import { EventType } from "@/enums/eventType";

export default class EventModel {
    public id?: number | null;
    public title?: string | null;
    public description?: string | null;
    public eventDate?: Date | null;
    public type?:EventType | null;
    public image:any = [];
    public file:any = null;

    constructor(){
        this.init();
    }

    init(){
        this.id = 0;
        this.title = "";
        this.description = "";
        this.image = [];
        this.file = null;
        this.eventDate = null;
        this.type;
    }
}
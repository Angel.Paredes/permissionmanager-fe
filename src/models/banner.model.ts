import { EventType } from "@/enums/eventType";

export default class Banner {
    public id?: number | null;
    public title?: string | null;
    public description?: string | null;
    public image:any = [];
    public file:any = null;

    constructor(){
        this.init();
    }

    init(){
        this.id = 0;
        this.title = "";
        this.description = "";
        this.image = [];
        this.file = null;
    }
}
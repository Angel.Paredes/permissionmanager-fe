var ODataQueryOption = /** @class */ (function () {
    function ODataQueryOption() {
        this.$expand = undefined;
        this.$filter = undefined;
        this.$orderby = undefined;
        this.$top = Number.parseFloat(process.env.VUE_APP_PAGE_SIZE) || 10;
        this.$skip = undefined;
        this.$select = undefined;
        this.$search = undefined;
        this.$count = true;
    }
    return ODataQueryOption;
}());
export default ODataQueryOption;
//# sourceMappingURL=ODataQueryOption.model.js.map
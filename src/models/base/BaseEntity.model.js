import { __extends } from "tslib";
import Base from './Base.model';
var BaseEntity = /** @class */ (function (_super) {
    __extends(BaseEntity, _super);
    function BaseEntity() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return BaseEntity;
}(Base));
export default BaseEntity;
//# sourceMappingURL=BaseEntity.model.js.map
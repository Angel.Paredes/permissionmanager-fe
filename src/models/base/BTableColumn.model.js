var BTableColumn = /** @class */ (function () {
    function BTableColumn(field, label, sortable) {
        if (sortable === void 0) { sortable = true; }
        this.field = field;
        this.label = label;
        this.sortable = sortable;
        this.label = this.label || this.field;
        this.visible = true;
    }
    return BTableColumn;
}());
export default BTableColumn;
//# sourceMappingURL=BTableColumn.model.js.map
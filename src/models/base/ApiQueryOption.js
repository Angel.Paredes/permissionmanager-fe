var ApiQueryOption = /** @class */ (function () {
    function ApiQueryOption($expand, where) {
        if ($expand === void 0) { $expand = undefined; }
        if (where === void 0) { where = undefined; }
        this.$expand = $expand;
        this.where = where;
        this.perpage = undefined;
        this.page = undefined;
        this.orderby = undefined;
        this.include = undefined;
        this.perpage = this.perpage || process.env.VUE_APP_PER_PAGE;
    }
    return ApiQueryOption;
}());
export default ApiQueryOption;
//# sourceMappingURL=ApiQueryOption.js.map
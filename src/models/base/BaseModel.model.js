var BaseModel = /** @class */ (function () {
    function BaseModel() {
        this.id = 0;
        this.deleted = false;
    }
    return BaseModel;
}());
export default BaseModel;
//# sourceMappingURL=BaseModel.model.js.map
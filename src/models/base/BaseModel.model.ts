export default class BaseModel {
    id: number;
    deleted: boolean;
    isDefault!: boolean;
    [key:string]: any;
    constructor() {
        this.id = 0;
        this.deleted = false;
    }
}
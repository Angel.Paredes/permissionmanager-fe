
export default class StreamVideoModel {
    public id?: number | null;
    public liveUrl?: string | null;
    public playerOneId?: number | null;
    public playerTwoId?: number | null;

    constructor(){
        this.init();
    }

    init(){
        this.id = null;
        this.liveUrl = null;
        this.playerOneId = null;
        this.playerTwoId = null;
    }
}
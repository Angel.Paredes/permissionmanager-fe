import { RankingType } from "@/enums/rankingType";

export default class RankingModel {

    public id?: number | null;
    public playerId?: number | null;
    public player2Id?: number | null;
    public associationId?: number | null;
    public point?: number | null;
    public playerRanking?: number | null;
    public rankingType?: RankingType | null;
    public rankingTypeName?: string | null;
    public playerName?: string | null;
    public secondPlayerName?: string | null;

    constructor(){
        this.init();
    }

    init(){
        this.id = 0;
        this.playerId = 0;
        this.player2Id = null;
        this.point = 0;
        this.playerRanking = 0;
        this.rankingType = RankingType.MALESINGLE;
    }
}